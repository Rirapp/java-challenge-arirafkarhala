package com.mycompany.ariraf.service;

import com.mycompany.ariraf.DTO.userDTO;
import com.mycompany.ariraf.entity.Users;
import com.mycompany.ariraf.record.Request.UsersLoginRequest;
import com.mycompany.ariraf.record.Request.UsersRegisterRequest;
import com.mycompany.ariraf.repository.UsersRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service

public class UserService {
    private final ResponeService responeService;
    private final UsersRepository usersRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public Object UserRegisterService(UsersRegisterRequest usersRegisterRequest){
        try {
            Users usersData = usersRepository.findByUserName(usersRegisterRequest.UserName());

            if (usersData !=null) {System.out.print("IF "+usersData);
                return responeService.GetServiceObject(false,"Failed Register", null);
            } else {
                Users Emty = new Users();
                String encodedPassword = bCryptPasswordEncoder.encode(usersRegisterRequest.Password());

                Emty.setPassword(encodedPassword);
                Emty.setCreatedAt(LocalDateTime.now());
                Users NewData = usersRepository.saveAndFlush(Emty);
//                String Token = JwtToken.GenToken(String.valueOf(NewData.getUserId()));

                return responeService.GetServiceObject(true,"Successfully Register", NewData);
            }
        }catch(Exception e){
            return responeService.GetServiceObject(false,"Failed Register", null);
        }
    }

    public Object UserLoginService(UsersLoginRequest usersLoginRequest ){

        try {
            Users usersData = usersRepository.findByUserName(usersLoginRequest.UserName());
            if(usersData == null  ){
                return responeService.GetServiceObject(false," 401 Failed Login, Username or Password is Empty", null);
            }
            else {
                return responeService.GetServiceObject(true,"Successfully Login", usersData);
            }
        }catch(Exception e){
            return responeService.GetServiceObject(false,"Failed Login", null);
        }
    }


    public Object getListUser() {
        try {

            List<Users> listUser =   usersRepository.getAll();
            if (listUser != null){
                return responeService.GetServiceObject(true,"  SUKSES", listUser);
            }else {
                return responeService.GetServiceObject(false," ERROR", null);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Object updateUser(userDTO payload) {
        try {

        Users cekData = usersRepository.getUser();

        if (cekData.getUserName() != null) {
            return responeService.GetServiceObject(false," 409 Username Sudah Terpakai", null);

        } else if (cekData.getPassword() != null) {
            return responeService.GetServiceObject(false," 400 Password tidak boleh sama dengan password sebelumnya", null);
        }

        if (cekData == null) {
            Users data = new Users();
            data.setUserName(payload.getUsername());
            String encodedPassword = bCryptPasswordEncoder.encode(payload.getPassword());
            data.setPassword(encodedPassword);
            usersRepository.save(data);
        }
            return responeService.GetServiceObject(true,"200 SUKSES",payload);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
