package com.mycompany.ariraf.controller;


import com.mycompany.ariraf.DTO.userDTO;
import com.mycompany.ariraf.record.Request.UsersLoginRequest;
import com.mycompany.ariraf.record.Request.UsersRegisterRequest;
import com.mycompany.ariraf.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("app")
public class AppController {
    private final UserService usersService;

    //regis
    @PostMapping("register")
    public Object RegisterController( @RequestBody(required = true) UsersRegisterRequest usersRegisterRequest) {
        return usersService.UserRegisterService(usersRegisterRequest);
    }
    //regis
    @PostMapping("login")
    public Object LoginController( @RequestBody(required = true) UsersLoginRequest usersLoginRequest) {
        return usersService.UserLoginService(usersLoginRequest);
    }

    //get list user
    @GetMapping("user/get_user_list")
    public ResponseEntity<?> getListUser() {

        return ResponseEntity.ok(usersService.getListUser());

    }

    //update user
    @PutMapping("user/update_user")
    public ResponseEntity<?> updateUser(@RequestBody userDTO payload) {

        return ResponseEntity.ok(usersService.updateUser(payload));

    }

}
