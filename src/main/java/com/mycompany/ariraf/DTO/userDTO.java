package com.mycompany.ariraf.DTO;


import lombok.*;

@Setter
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class userDTO {
    Long id;
    String username;
    String password;
}
