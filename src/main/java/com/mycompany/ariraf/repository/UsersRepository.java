package com.mycompany.ariraf.repository;




import com.mycompany.ariraf.entity.Users;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
@Repository

public interface UsersRepository extends JpaRepository<Users, UUID> {
    @Query(value="SELECT * FROM users", nativeQuery = true)
    Users findByUserName(String UserName);

    @Query(value="Select * from Users", nativeQuery = true)
    List<Users> getAll();

    @Query(value="Select * from Users", nativeQuery = true)
    Users getUser();
}
