package com.mycompany.ariraf.ariraf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArirafApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArirafApplication.class, args);
	}

}
