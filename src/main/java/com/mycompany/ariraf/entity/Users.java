package com.mycompany.ariraf.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Users {

    @Id
    @Column(length=25)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length=25, nullable=false, unique=true)
    private String userName;

    @Column(length=25, nullable=false)
    private String password;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;


}
